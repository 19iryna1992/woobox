<?php

/**
 * Template Name: Contact Form
 *
 * Template for displaying contact form.
 *
 * @package woofbox
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;
get_header();

$form_shortcode = get_field('contact_form_shortcode');
?>


<main id="main" role="main" tabindex="-1">

    <section class="s-contact-page">

        <?php if ($form_shortcode) : ?>
            <div class="s-contact-page__form">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-6 col-lg-5 col-xl-auto">
                            <div class="s-contact-page__image">
                                <?php if (has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('full'); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-5">
                            <h1 class="s-contact-page__title text-uppercase"><?php the_title(); ?></h1>
                            <div class="s-contact-page__form">
                                <div class="c-form">
                                    <?php echo do_shortcode($form_shortcode); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </section>


</main>

<?php get_footer(); ?>
