<?php /* Template Name: Home */

if (!defined('ABSPATH')) exit;

get_header();
?>

<main id="main" role="main" tabindex="-1">

  <?php get_template_part('template-parts/sections/home/hero-banner'); ?>

  <?php get_template_part('template-parts/sections/home/woof-boxes'); ?>

  <?php get_template_part('template-parts/sections/home/testimonials'); ?>

  <?php get_template_part('template-parts/sections/home/what-inside'); ?>

  <?php get_template_part('template-parts/sections/home/instagram'); ?>

  <?php get_template_part('template-parts/sections/home/contact-banner'); ?>



</main>

<?php get_footer(); ?>