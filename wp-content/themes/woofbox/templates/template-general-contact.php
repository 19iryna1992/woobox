<?php

/**
 * Template Name: General Contact Info
 *
 * Template for displaying general contact info.
 *
 * @package woofbox
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;
get_header(); ?>

<main id="main" role="main" tabindex="-1">

    <section class="s-general-contact">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="s-default-page__title text-center"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <?php if (have_rows('contact_menu')):
            $title = get_field('contact_menu_title');
            ?>
            <div class="s-general-contact__nav">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="c-section-intro__description text-center"><?php echo $title; ?></h2>
                        </div>
                    </div>
                    <div class="row">
                        <?php while (have_rows('contact_menu')): the_row();
                            $button = get_sub_field('button');
                            $button_desc = get_sub_field('button_bottom_description');
                            ?>
                            <div class="col-md-6 col-lg-4 text-center">
                                <?php if ($button) : ?>
                                    <a href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>"
                                       class="c-button c-button--outline"><?php echo $button['title']; ?></a>
                                <?php endif; ?>
                                <?php if ($button_desc) : ?>
                                    <p class="c-button-desc"><?php echo $button_desc; ?></p>
                                <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>

        <?php endif; ?>

    </section>

</main>
<?php get_footer(); ?>
