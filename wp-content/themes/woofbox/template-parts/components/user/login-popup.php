<div id="login-popup" class="c-login-popup zoom-anim-dialog mfp-hide">
    <div class="c-login-popup__container">
        <div class="c-login-popup__intro">
            <h3 class="c-login-popup__title">
                <?php _e('Zaloguj się','woofbox'); ?>
            </h3>
        </div>
        <div class="c-login-popup__form">
            <?php woocommerce_login_form(array(
                'redirect' =>  wc_customer_edit_account_url()
            )); ?>
        </div>

    </div>

</div>

