<?php
$instagram = get_field('company_instagram', 'option');
$facebook = get_field('company_facebook', 'option');
$youtube = get_field('company_youtube', 'option');
?>

<div class="c-social-menu">
    <ul class="c-social-menu__list">
        <?php if ($instagram) : ?>
            <li class="c-social-menu__item">
                <a href="<?php echo $instagram; ?>" target="_blank" class="c-social-menu__link">
                    <i class="fab fa-instagram"></i>
                </a>
            </li>
        <?php endif; ?>
        <?php if ($facebook) : ?>
            <li class="c-social-menu__item">
                <a href="<?php echo $facebook; ?>" target="_blank" class="c-social-menu__link">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </li>
        <?php endif; ?>
        <?php if ($youtube) : ?>
            <li class="c-social-menu__item">
                <a href="<?php echo $youtube; ?>" target="_blank" class="c-social-menu__link">
                    <i class="fab fa-youtube"></i>
                </a>

            </li>
        <?php endif; ?>
    </ul>
</div>