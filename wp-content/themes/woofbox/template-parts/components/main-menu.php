<div class="c-main-menu">
    <?php
    wp_nav_menu(array(
        'theme_location' => 'primary-menu',
        'menu_id' => 'primary-menu',
        'container'       => 'nav',
        'container_class' => 'c-main-menu__container',
        'menu_class'      => 'c-main-menu__list',
    ));
    ?>
    <ul class="c-main-menu__secondary">
        <?php if(!is_user_logged_in()) : ?>
        <li class="menu-item menu-item--login">
            <a href="#login-popup" class="JS--open-login"><?php _e('Zaloguj się','woofbox'); ?></a>
        </li>
        <?php else : ?>
            <li class="menu-item menu-item--login">
                <a href="<?php echo wc_customer_edit_account_url(); ?>" ><?php _e('My account','woofbox'); ?></a>
            </li>
        <?php endif; ?>
    </ul>
</div>

