<?php

$section_title = get_field('subscribe_title', 'option');
$form = get_field('subscribe_form', 'option');
?>
<?php if($form) :?>
<section class="s-subscribe u-bg-secondary">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <?php if ($section_title) : ?>
                <div class="col-lg-12">
                    <div class="s-subscribe__intro">
                        <h3 class="c-section-intro__title--big u-white"><?php echo $section_title; ?></h3>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-lg-12">
                <div class="s-subscribe__form">
                    <?php echo do_shortcode($form); ?>
                </div>

            </div>

        </div>
    </div>
</section>
<?php endif; ?>