<?php if (have_rows('single_product_boxes')): ?>
    <section class="s-product-boxes">
        <div class="container">
            <?php
            $boxes_counter = 1;
            while (have_rows('single_product_boxes')): the_row();
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                ?>
                <div class="row align-items-center">
                    <div class="col-md-6 col-lg-5">
                        <?php if ($image) : ?>
                            <div class="s-product-boxes__image">
                                <?php echo wp_get_attachment_image($image['ID'], 'full'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6 col-lg-7">
                        <?php if ($title || $description) : ?>
                            <div class="s-product-boxes__content">
                                <?php if ($title) : ?>
                                    <h2 class="s-product-boxes__title"><?php echo $title ?></h2>
                                <?php endif; ?>
                                <?php if ($description) : ?>
                                    <div class="s-product-boxes__description">
                                        <?php echo $description; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <?php $boxes_counter++; endwhile; ?>

        </div>
    </section>
<?php endif; ?>









