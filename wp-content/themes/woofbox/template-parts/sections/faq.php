<?php
$title = get_field('faq_main_title');

if (have_rows('faq_list')): ?>

    <section class="s-faq">
        <?php if ($title) : ?>
            <div class="s-faq__intro">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="s-faq__title text-center"><?php echo $title; ?></h2>
                        </div>
                    </div>
                </div>
            </div>

        <?php endif; ?>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-9">
                    <div class="c-faq">
                        <?php
                        $faq_counter = 1;
                        while (have_rows('faq_list')): the_row();
                            $title = get_sub_field('title');
                            $content = get_sub_field('description');
                            ?>
                            <div class="c-faq__item JS--faq-item">
                                <div class="c-faq__heading-wrap  JS--faq-title <?php echo ($faq_counter == 1) ? 'is-active' : null; ?>">
                                    <h3 class="c-faq__heading"><?php echo $title; ?></h3>
                                    <span class="c-faq__heading-icon"></span>
                                </div>
                                <div class="c-faq__body u-primary-200 JS--faq-content">
                                    <?php echo $content; ?>
                                </div>
                            </div>
                        <?php $faq_counter++; endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>