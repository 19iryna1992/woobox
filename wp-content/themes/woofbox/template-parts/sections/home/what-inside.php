<?php

//cta box
$cta_img = get_field('how_cta_image');
$cta_title = get_field('how_cta_title');
$cta_desc = get_field('how_cta_description');
$cta_btn = get_field('how_cta_btn');
?>


<section class="s-what-inside">
    <div class="c-box-cta">
        <div class="container">
            <?php if ($cta_title) : ?>
            <div class="row">
                <div class="col-12">
                    <div class="s-what-inside__intro">
                        <h2 class="c-section-intro__title--big text-center">
                            <?php echo $cta_title; ?>
                        </h2>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-md-6 col-lg-4 order-2 order-md-1">
                    <div class="c-box-cta__content">
                        <?php if ($cta_desc) : ?>
                            <div class="c-section-intro__description">
                                <?php echo $cta_desc; ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($cta_btn) : ?>
                            <div class="c-box-cta__btn">
                                <a href="<?php echo $cta_btn['url']; ?>" target="<?php echo $cta_btn['target']; ?>"
                                   class="c-button"><?php echo $cta_btn['title']; ?></a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-6 order-1 order-md-2">
                    <?php if ($cta_img) : ?>
                        <div class="c-box-cta__image">
                            <?php echo wp_get_attachment_image($cta_img['ID'], 'full'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</section>