<?php

//vars
$cta_title = get_field('contact_cta_title');
$cta_desc = get_field('contact_cta_description');
$cta_btn = get_field('contact_cta_button');

$cta_img_first = get_field('contact_cta_image_first');
$cta_img_second = get_field('contact_cta_image_second');
?>
<section class="s-contact-banner">
    <div class="s-contact-banner__title">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php if ($cta_title) : ?>
                        <h2 class="c-section-intro__title--big text-center u-secondary"><?php echo $cta_title; ?></h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="c-box-cta">

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-3 col-lg-3 col-xl-4">
                    <div class="c-box-cta__image">
                        <?php if ($cta_img_first) : ?>
                            <?php echo wp_get_attachment_image($cta_img_first['ID'], 'full'); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-md-5 col-lg-5 col-xl-4">
                    <div class="s-contact-banner__intro">
                        <div class="c-box-cta__content text-center">
                            <?php if ($cta_desc) : ?>
                                <div class="c-section-intro__description"><?php echo $cta_desc; ?></div>
                            <?php endif; ?>
                            <?php if ($cta_btn) : ?>
                                <a href="<?php echo $cta_btn['url']; ?>" target="<?php echo $cta_btn['target']; ?>"
                                   class="c-button"><?php echo $cta_btn['title']; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-xl-4">
                    <div class="c-box-cta__image s-contact-banner__secondimg">
                        <?php if ($cta_img_second) : ?>
                            <?php echo wp_get_attachment_image($cta_img_second['ID'], 'full'); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>