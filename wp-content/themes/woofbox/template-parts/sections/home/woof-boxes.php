<?php
$how_title = get_field('how_title');

//cta box
$cta_img = get_field('how_cta_image');
$cta_title = get_field('how_cta_title');
$cta_desc = get_field('how_cta_description');
$cta_btn = get_field('how_cta_btn');
?>
<?php if (have_rows('how_boxes')) : ?>
    <section class="s-woofboxes">
        <?php if ($how_title) : ?>

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="s-woofboxes__intro text-center">
                            <h2 class="c-section-intro__title--big"><?php echo $how_title; ?></h2>
                        </div>
                    </div>
                </div>
            </div>

        <?php endif; ?>

        <div class="container">
            <div class="row justify-content-lg-between">

                <?php while (have_rows('how_boxes')) : the_row();
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                    ?>
                    <div class="col-md-4 col-lg-4 py-3">
                        <div class="c-image-box">
                            <div class="c-image-box__thumb">

                                <?php if ($image) : ?>
                                    <img src="<?php echo esc_url($image['url']); ?>"
                                         alt="<?php echo esc_attr($image['alt']); ?>"/>
                                <?php endif; ?>
                            </div>
                            <?php if ($title) : ?>
                                <div class="c-image-box__content">
                                    <h3 class="c-image-box__title">
                                        <?php echo $title; ?>
                                    </h3>
                                    <div class="c-image-box__description">
                                        <?php echo $description; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>

            </div>
        </div>
    </section>

<?php endif; ?>


