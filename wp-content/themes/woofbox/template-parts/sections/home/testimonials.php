<?php

$title = get_field('testimonials_section_title');
$image = get_field('testimonials_section_image');
?>
<?php if (have_rows('testimonials_slider')): ?>
    <section class="s-testimonials">
        <?php if ($title) : ?>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-testimonials__intro text-center">
                        <h2 class="c-section-intro__title--big"><?php echo $title; ?></h2>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-6 col-lg-5 s-testimonials__column">
                    <div class="s-testimonials__img">
                        <?php
                        if ($image) {
                            echo wp_get_attachment_image($image['ID'], 'full');
                        }

                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 s-testimonials__column">
                    <div class="s-testimonials__slider">
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-container JS--testimonials-slider">

                            <div class="swiper-wrapper">
                                <?php while (have_rows('testimonials_slider')): the_row();
                                    $testimonial_text = get_sub_field('testimonial_text');
                                    $testimonial_author = get_sub_field('testimonial_author');
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="c-testimonials">
                                            <?php if ($testimonial_text) : ?>
                                                <div class="c-testimonials__text">
                                                    <?php echo $testimonial_text; ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if ($testimonial_author) : ?>
                                                <div class="c-testimonials__author">
                                                    <?php echo $testimonial_author; ?>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>

                                <?php endwhile; ?>

                            </div>

                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-next"></div>
                    </div>

                </div>
            </div>
        </div>
    </section>
<?php endif; ?>





