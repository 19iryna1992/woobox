<?php
$title = get_field('hero_banner_main_title');
$hero_banner_image = get_field('hero_banner_image');
$hero_mobile_banner = get_field('home_mobile_banner');

// banner cta box

$cta_title = get_field('hero_banner_cta_title');
$cta_desc = get_field('hero_banner_cta_description');
$cta_img = get_field('hero_banner_cta_image');
?>
<section class="s-hero-banner">
    <div class="s-hero-banner__top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="s-hero-banner__content u-bg-img s-hero-banner__content--desktop"
                         style="background-image: url('<?php echo ($hero_banner_image) ? $hero_banner_image['url'] : 'https://fakeimg.pl/1140x360/?text=Banner'; ?>')">
                        <div class="s-hero-banner__btn">
                            <a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>"
                               class="c-button"><?php _e('Wybierz swoją paczkę', 'woofbox'); ?></a>
                            <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                               class="c-button">Zarejestruj się!</a>
                        </div>

                    </div>

                    <div class="s-hero-banner__content--mobile">
                        <div class="s-hero-banner__content--mobile__image">
                            <?php echo wp_get_attachment_image($hero_mobile_banner['ID'], 'full'); ?>
                        </div>
                        <div class="s-hero-banner__btn">
                            <a href="<?php echo get_permalink(woocommerce_get_page_id('shop')); ?>"
                               class="c-button"><?php _e('Wybierz swoją paczkę', 'woofbox'); ?></a>
                            <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                               class="c-button">Zarejestruj się!</a>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
</section>


<div class="c-box-cta">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-lg-4">
                <div class="c-box-cta__content">
                    <h2 class="c-section-intro__title"><?php echo $cta_title; ?></h2>
                    <div class="c-section-intro__description">
                        <?php echo $cta_desc; ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <?php if ($cta_img) : ?>
                    <div class="c-box-cta__image">
                        <?php echo wp_get_attachment_image($cta_img['ID'], 'full'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

