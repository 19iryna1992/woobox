<?php

//var
$insta_shortcode = get_field('instagram_shortcode');

?>
<section class="s-instagram">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="s-instagram__intro">
                    <h2 class="c-section-intro__title--big text-center">nasz instagram</h2>
                </div>
            </div>
            <div class="col-12">
                <div class="s-instagram__gallery">
                    <?php echo do_shortcode($insta_shortcode); ?>
                </div>
            </div>
        </div>
    </div>
</section>