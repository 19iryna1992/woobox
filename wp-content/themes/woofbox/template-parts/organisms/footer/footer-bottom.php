<div class="o-footer__bottom">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-2 order-2 order-md-1">
                <div class="o-footer__copyright">
                    <?php echo "© " . date('Y') . ' ' . get_bloginfo('name'); ?>
                </div>
            </div>
            <div class="col-12 col-md-10 order-1 order-md-2">
                <div class="o-footer__bottom-menu">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer-menu',
                        'menu_id' => 'footer-menu',
                        'container'       => 'nav',
                        'container_class' => 'c-bottom-footer-menu',
                        'menu_class'      => 'c-bottom-footer-menu__list',
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>