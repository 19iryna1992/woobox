<div class="o-footer__top">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4">
                <div class="o-footer__socials">
                    <!--<h2 class="c-widget__title">Dołącz do nas</h2>-->
                    <?php get_template_part('template-parts/components/social-menu'); ?>
                </div>
            </div>
    <!--        <div class="col-md-6 col-lg-3">
                <?php /*if (is_active_sidebar('footer-3')) :
                    dynamic_sidebar('footer-3');
                endif;
                */?>
            </div>-->
            <div class="col-6 col-md-4">
                <?php if (is_active_sidebar('footer-1')) :
                    dynamic_sidebar('footer-1');
                endif;
                ?>
            </div>
            <div class="col-6 col-md-4">
                <?php if (is_active_sidebar('footer-2')) :
                    dynamic_sidebar('footer-2');
                endif;
                ?>
            </div>


        </div>
    </div>
</div>