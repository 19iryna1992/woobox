<div class="container">
    <div class="row justify-content-between align-items-center">
        <div class="col-auto">
            <div class="o-header__logo">
                <?php the_custom_logo(); ?>
            </div>
        </div>
        <div class="col-auto">
            <div class="o-header__mobile-btn JS--mobile-menu-btn">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="o-header__menu JS--main-menu">
                <?php get_template_part('template-parts/components/main-menu'); ?>
            </div>
        </div>
    </div>
</div>