<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package woofbox
 */

if (!defined('ABSPATH')) exit;

?>

</div><!-- #content -->
<?php get_template_part('template-parts/sections/subscribe'); ?>
<footer class="o-footer">
    <?php get_template_part('template-parts/organisms/footer/footer-top'); ?>
    <?php //get_template_part('template-parts/organisms/footer/footer-bottom'); ?>
</footer>

<?php //if (!is_user_logged_in() && !is_page('my-account')) : ?>
<?php get_template_part('template-parts/components/user/login-popup'); ?>
<?php //endif; ?>
</div><!-- #page -->
<?php wp_footer(); ?>
<script src="//code.tidio.co/kyulypuuxuhpn7ngpx2mkjghjkk6dlag.js" async></script>
</body>

</html>