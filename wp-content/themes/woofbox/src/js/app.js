global.$ = global.jQuery = require('jquery');

require('./components/global');
require('./components/mobile-menu');
require('./components/registration');
require('./components/single-product');
require('./components/sliders');
require('./components/popups');
require('./components/faq');
