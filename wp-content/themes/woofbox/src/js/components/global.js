$(document).ready(function () {


    $(window).scroll(function(){

        var stickyHeader = $('.o-header');
        var scroll = $(window).scrollTop();

        if (scroll >= 10) {
            stickyHeader.addClass('fixed');
        } else {
            stickyHeader.removeClass('fixed');
        }
    });

    $('#dog-image').on('change',function(){
        previewFile();
    });

});


function previewFile() {
    var preview = document.getElementById('avatar-preview');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        // convert image file to base64 string
        preview.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}


