import Swiper from 'swiper';
import { Autoplay } from 'swiper/js/swiper.esm';

$(document).ready(function () {

    var testimonialsSliderContainer = $('.JS--testimonials-slider');

    if(testimonialsSliderContainer.length !== 0) {
        var testimonialsSwiper = new Swiper((testimonialsSliderContainer), {
            noSwipingClass: 'swiper-no-swiping',
            loop:true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable:true,
            }
        })
    }

});