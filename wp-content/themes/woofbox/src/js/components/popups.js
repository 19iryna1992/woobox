require("magnific-popup");
$(document).ready(function () {

    $(".JS--open-login").magnificPopup({
        type: 'inline',
        midClick: true,
        showCloseBtn: true,
        fixedContentPos: true,
        overflowY: 'auto',
        fixedBgPos: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in c-login-popup-wrapper',
        callbacks: {
            open: function () {

            },
            close: function () {

            }
        }
    });


});