$(document).ready(function () {

    var mobileMenuBtn = $('.JS--mobile-menu-btn');
    var mainMenu = $('.JS--main-menu');

    mobileMenuBtn.on('click', function () {
        $(this).toggleClass('is-open');
        mainMenu.slideToggle();
    });


});