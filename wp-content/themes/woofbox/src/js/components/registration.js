$(document).ready(function () {

    var regSteps = $('.JS--register-step');
    var stepNextBtn = $('.JS--reg-next-step-btn');
    var stepPrevBtn = $('.JS--reg-prev-step-btn');

    if (regSteps.length !== 0) {

        validationOnChange();

        stepNextBtn.on('click', function (e) {
            e.preventDefault();
            let currentStep = $(this).attr('data-step');
            let validVal = validationStep(currentStep);
            if (validVal) {
                goToStep((currentStep * 1) + 1);
                if (currentStep == 1) {
                    let dogName = $('#dogs_name').val();
                    $('.JS--dog-name').text(dogName);
                }
            }
        });

        stepPrevBtn.on('click', function (e) {
            e.preventDefault();
            let currentStep = $(this).attr('data-step');
            goToStep((currentStep * 1) - 1);

        });

    }


    function goToStep(step) {
        regSteps.filter('[data-register-step!=' + step + ']').fadeOut(0);
        regSteps.filter('[data-register-step=' + step + ']').fadeIn(300);
    }

    function validationStep(step) {
        let valid = true;
        regSteps.filter('[data-register-step=' + step + ']').find('input').each(function () {
            if ($(this).prop('required')) {
                if (!$(this).val()) {
                    $(this).addClass('is-invalid');
                    stepNextBtn.filter('[data-step=' + step + ']').addClass('is-disabled');
                    valid = false;
                } else {
                    $(this).removeClass('is-invalid');
                    stepNextBtn.filter('[data-step=' + step + ']').removeClass('is-disabled');
                    valid = true;
                }
            }
        });

        return valid;
    }


    function validationOnChange() {
        regSteps.each(function () {
            var currentStep = $(this).attr('data-register-step');
            $(this).find('input').each(function () {
                if ($(this).prop('required')) {
                    $(this).on('input', function () {
                        if (!$(this).val() || $(this).val().replace(/^\s+|\s+$/g, "").length == 0) {
                            $(this).addClass('is-invalid');
                            stepNextBtn.filter('[data-step=' + currentStep + ']').addClass('is-disabled');
                        } else {
                            $(this).removeClass('is-invalid');
                            stepNextBtn.filter('[data-step=' + currentStep + ']').removeClass('is-disabled');
                        }
                    });
                }
            });
        });


    }


});






