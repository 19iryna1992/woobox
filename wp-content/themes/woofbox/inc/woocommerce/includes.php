<?php

if (!defined('ABSPATH')) exit;

require_once 'registration.php';
require_once 'my-account.php';
require_once 'checkout.php';
require_once 'hooks.php';