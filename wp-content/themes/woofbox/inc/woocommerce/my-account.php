<?php

if (!defined('ABSPATH')) exit;

// ------------------
// 1. Register new endpoint to use for My Account page
// Note: Resave Permalinks or it will give 404 error

function woof_pass_reset_tab()
{
    add_rewrite_endpoint('password-change', EP_ROOT | EP_PAGES);
}

add_action('init', 'woof_pass_reset_tab');


// ------------------
// 2. Add new query var

function woof_add_pass_reset_query_vars($vars)
{
    $vars[] = 'password-change';
    return $vars;
}

add_filter('query_vars', 'woof_add_pass_reset_query_vars', 0);


// ------------------
// 3. Insert the new endpoint into the My Account menu

function woof_tabs_my_account($items)
{
    $items['orders'] = 'MOJE ZAMÓJENIA';
    $items['password-change'] = 'MOJE KONTO';
    $items['edit-account'] = 'MÓJ PIES';
    $items['edit-address'] = 'ADRES';

    return $items;
}

add_filter('woocommerce_account_menu_items', 'woof_tabs_my_account',999);


// ------------------
// 4. Add content to the new endpoint

function woof_pass_reset_content()
{
    get_template_part('template-parts/woocommerce/change-password-form');

}

add_action('woocommerce_account_password-change_endpoint', 'woof_pass_reset_content');


