<?php

if (!defined('ABSPATH')) exit;

// add fields
function reigel_woocommerce_checkout_fields( $checkout_fields = array() ) {

    $checkout_fields['order']['deliverymethod'] = array(
        'type'          => 'select',
        'class'         => array('select-field', 'form-row-wide','select-field--delivery'),
        'label'         => __('Wybierz metodę dostawy'),
        'label_class'   => 'delivery-label',
        'required'      => true,
        'options'	=> array( // options for <select> or <input type="radio" />
            ''		=> 'Metodę dostawy', // empty values means that field is not selected
            'Kurier'	=> 'kurier', // 'value'=>'Name'
            'Paczkomat'	=> 'paczkomat'
        )
    );

    return $checkout_fields;
}
add_filter( 'woocommerce_checkout_fields', 'reigel_woocommerce_checkout_fields' );


// save fields to order meta
add_action( 'woocommerce_checkout_update_order_meta', 'woof_save_checkout_new_fields' );

// save new fields values
function woof_save_checkout_new_fields( $order_id ){

    if( !empty( $_POST['deliverymethod'] ) ){
        update_post_meta( $order_id, 'deliverymethod', $_POST['deliverymethod'] );
    }
}


function woo_woocommerce_checkout_update_user_meta( $customer_id, $posted ) {
    if (isset($posted['deliverymethod'])) {
        $dob = sanitize_text_field( $posted['deliverymethod'] );
        update_user_meta( $customer_id, 'deliverymethod', $dob);
    }
}
add_action( 'woocommerce_checkout_update_user_meta', 'woo_woocommerce_checkout_update_user_meta', 10, 2 );

add_action( 'woocommerce_admin_order_data_after_billing_address', 'woo_checkout_field_display_admin_order_meta', 10, 1 );

function woo_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Metodę dostawy').':</strong> ' . get_post_meta( $order->get_id(), 'deliverymethod', true ) . '</p>';
}

add_action('woocommerce_checkout_process', 'domain_checkout_field_process');
function domain_checkout_field_process() {
    // Check if it's set and if it's not set, we add an error.
    if ( ! $_POST['deliverymethod'] )
        wc_add_notice( __( 'Wybierz metodę dostawy.' ), 'error' );
}