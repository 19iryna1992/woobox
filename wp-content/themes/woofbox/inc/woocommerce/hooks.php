<?php

if (!defined('ABSPATH')) exit;

/**
 * Redirect users after add to cart.
 */
function woof_add_to_cart_redirect($url)
{


    $url = wc_get_checkout_url();

    return $url;

}

//add_filter( 'woocommerce_add_to_cart_redirect', 'woof_add_to_cart_redirect' );

/**
 * Remove uncategorized from the WooCommerce breadcrumb.
 *
 * @param Array $crumbs Breadcrumb crumbs for WooCommerce breadcrumb.
 * @return Array   WooCommerce Breadcrumb crumbs with default category removed.
 */
function woof_wc_remove_uncategorized_from_breadcrumb($crumbs)
{
    $category = get_option('default_product_cat');
    $caregory_link = get_category_link($category);

    foreach ($crumbs as $key => $crumb) {
        if (in_array($caregory_link, $crumb)) {
            unset($crumbs[$key]);
        }
    }

    return array_values($crumbs);
}

add_filter('woocommerce_get_breadcrumb', 'woof_wc_remove_uncategorized_from_breadcrumb');


// To change add to cart text on single product page
add_filter('woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text');
function woocommerce_custom_single_add_to_cart_text()
{
    return __('kup teraz', 'woocommerce');
}

// To change add to cart text on product archives(Collection) page
add_filter('woocommerce_product_add_to_cart_text', 'woocommerce_custom_product_add_to_cart_text');
function woocommerce_custom_product_add_to_cart_text()
{
    return __('Buy Now', 'woocommerce');
}


// Remove sidebar
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);


/**
 * @desc Remove in all product type
 */
function wc_remove_all_quantity_fields($return, $product)
{
    return true;
}

//add_filter( 'woocommerce_is_sold_individually', 'wc_remove_all_quantity_fields', 10, 2 );


//remove fields from single product

//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
add_action( 'woocommerce_after_single_product', 'woocommerce_template_single_add_to_cart', 12 );

//remove fields from Archive products
remove_action('woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);

remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
add_action('woocommerce_after_shop_loop_item', 'woof_view_product_button', 10);

function woof_view_product_button()
{
    global $product;
    $link = $product->get_permalink();
    echo '<a href="' . $link . '" class="c-button o-archive-product__link-view">Zajrzyj do środka</a>';
}

add_filter('single_product_archive_thumbnail_size', function ($size) {
    return 'full';
});


/**
 * Remove the breadcrumbs
 */
add_action('init', 'woo_remove_wc_breadcrumbs');
function woo_remove_wc_breadcrumbs()
{
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
}


add_filter('woocommerce_single_product_image_thumbnail_html', 'wc_remove_link_on_thumbnails');

function wc_remove_link_on_thumbnails($html)
{
    return strip_tags($html, '<img>');
}

add_filter('woocommerce_save_account_details_required_fields', 'woof_myaccount_required_fields');

function woof_myaccount_required_fields($account_fields)
{

    unset($account_fields['account_last_name']);

    unset($account_fields['account_first_name']);
    unset($account_fields['account_display_name']);

    return $account_fields;

}

/**
 * Remove title in product card
 */

add_action('init', 'woo_remove_wc_item_title');
function woo_remove_wc_item_title()
{
    remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
}


add_action('woocommerce_before_shop_loop_item_title', 'woof_template_loop_product_thumbnail', 10);

function woof_template_loop_product_thumbnail()
{
    global $product;
    $mobile_image = get_field('product_mobile_image', $product->get_id());
    if($mobile_image){
        echo '<div class="o-archive-product-mobile-image">'.wp_get_attachment_image($mobile_image['ID'],'full').'</div>';
    }
}


/**
 * Notify admin when a new customer account is created
 */
add_action( 'woocommerce_created_customer', 'woocommerce_created_customer_admin_notification' );
function woocommerce_created_customer_admin_notification( $customer_id ) {
    wp_send_new_user_notifications( $customer_id, 'admin' );
}



/** *
 *
 *Redirect non logged user
 *
 */

add_action('template_redirect', 'woocommerce_custom_redirections');
function woocommerce_custom_redirections() {
    // Case1: Non logged user on checkout page (cart empty or not empty)
    if ( !is_user_logged_in() && (is_checkout() || is_cart() ))
        wp_redirect( get_permalink( get_option('woocommerce_myaccount_page_id') ) );

    // Case2: Logged user on my account page with something in cart
    /*if( is_user_logged_in() && ! WC()->cart->is_empty() && is_account_page() )
        wp_redirect( get_permalink( get_option('woocommerce_checkout_page_id') ) );*/
}