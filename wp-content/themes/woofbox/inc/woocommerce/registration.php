<?php

if (!defined('ABSPATH')) exit;

function wooc_extra_register_fields()
{ ?>
    <div class="c-register__step JS--register-step is-active-step" data-register-step="1">
        <div class="c-register__row">
            <label for="dog_sex"><?php _e('Jak ma na imię twoje zwierzę?', 'woofbox'); ?> </label>
            <select name="dog_sex" id="dog_sex">
                <option value="boy"><?php _e('Nasz chłopak ma na imię', 'woofbox'); ?></option>
                <option value="girl"><?php _e('Nasza dziewczyna ma piękne imię.', 'woofbox'); ?></option>
            </select>
        </div>
        <div class="c-register__row">
            <input type="text" class="input-text" name="dogs_name" id="dogs_name" required
                   value="<?php if (!empty($_POST['dogs_name'])) esc_attr_e($_POST['dogs_name']); ?>"/>
        </div>
        <div class="c-register__footer c-register__footer--first">

            <?php renderRegStepBullets(1); ?>
            <a href="javascript:void(0);" data-step="1"
               class="c-button JS--reg-next-step-btn is-disabled"><?php _e('Następne', 'woofbox'); ?></a>
        </div>
    </div>

    <div class="c-register__step JS--register-step" data-register-step="2">
        <div class="c-radio-dog-size">
            <label>
                <input type="radio" value="small" name="dog-size" required>
                <div class="c-radio-dog-size__img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/dogs/small_dog_size.png" alt="small dog">
                </div>
                <div class="c-radio-dog-size__text"> Mały i piękny. 0-10 kg</div>
            </label>
            <label>
                <input type="radio" value="medium" name="dog-size" required>
                <div class="c-radio-dog-size__img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/dogs/medium_dog_size.png"
                         alt="medium dog">
                </div>
                <div> Zgadza się. 10-20 kg</div>
            </label>
            <label>
                <input type="radio" value="big" name="dog-size" required>
                <div class="c-radio-dog-size__img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/dogs/large_dog_size.png" alt="large dog">
                </div>
                <div class="c-radio-dog-size__text"> Duży i miły. 25 + kg</div>
            </label>
        </div>
        <div class="c-register__footer">
            <a href="javascript:void(0);" data-step="2"
               class="c-button c-button c-button--outline JS--reg-prev-step-btn"><?php _e('Poprzednie', 'woofbox'); ?></a>
            <?php renderRegStepBullets(2); ?>
            <a href="javascript:void(0);" data-step="2"
               class="c-button JS--reg-next-step-btn is-disabled"><?php _e('Następne', 'woofbox'); ?></a>
        </div>
    </div>

    <div class="c-register__step JS--register-step" data-register-step="3">
        <div class="c-register__row">
            <label for="breed"><?php _e('Jaki rodzaj rasy ', 'woofbox'); ?><span class="JS--dog-name"></span> ?</label>
            <input type="text" class="input-text" name="breed" id="breed" required
                   value="<?php if (!empty($_POST['breed'])) esc_attr_e($_POST['breed']); ?>"/>
        </div>
        <div class="c-register__footer">
            <a href="javascript:void(0);" data-step="3"
               class="c-button c-button c-button--outline JS--reg-prev-step-btn"><?php _e('Poprzednie', 'woofbox'); ?></a>
            <?php renderRegStepBullets(3); ?>
            <a href="javascript:void(0);" data-step="3"
               class="c-button JS--reg-next-step-btn is-disabled"><?php _e('Następne', 'woofbox'); ?></a>
        </div>
    </div>

    <div class="c-register__step JS--register-step" data-register-step="4">
        <div class="c-register__row">
            <label for="birthday"><?php _e('Kiedy są urodziny zwierzęcia sdsa ?', 'woofbox'); ?> </label>
            <input type="date" class="input-text" name="birthday" id="birthday"
                   value="<?php if (!empty($_POST['birthday'])) esc_attr_e($_POST['birthday']); ?>"/>
        </div>
        <div class="c-register__footer">
            <a href="javascript:void(0);" data-step="4"
               class="c-button c-button c-button--outline JS--reg-prev-step-btn"><?php _e('Poprzednie', 'woofbox'); ?></a>
            <?php renderRegStepBullets(4); ?>
            <a href="javascript:void(0);" data-step="4"
               class="c-button JS--reg-next-step-btn"><?php _e('Następne', 'woofbox'); ?></a>
        </div>
    </div>

    <div class="c-register__step JS--register-step" data-register-step="5">
        <div class="c-register-radio-list">
            <h2 class="c-register-radio-list__title"><?php _e('Jeśli jest uczulony?', 'woofbox'); ?></h2>
            <p class="c-register-radio-list__subtitle"><?php _e('Nie wysyłaj przysmaków zawierających...', 'woofbox'); ?></p>
            <div class="c-register-radio-list__item">
                <input type="radio" name="treats" value="chicken" id="chicken" class="c-register-radio-list__input"  required>
                <label for="chicken"><?php _e('Kurczak', 'woofbox'); ?></label>
            </div>
            <div class="c-register-radio-list__item">
                <input type="radio" name="treats" value="turkey" id="turkey" class="c-register-radio-list__input" required>
                <label for="turkey"><?php _e('Indyk', 'woofbox'); ?></label>
            </div>
            <div class="c-register-radio-list__item">
                <input type="radio" name="treats" value="beef" id="beef" class="c-register-radio-list__input" required>
                <label for="beef"><?php _e('Wołowina', 'woofbox'); ?></label>
            </div>
            <div class="c-register-radio-list__item">
                <input type="radio" name="treats" value="none" id="no_send_me_everything"
                       class="c-register-radio-list__input" required>
                <label for="no_send_me_everything"><?php _e('Nie, przyślij mi wszystko.', 'woofbox'); ?></label>
            </div>
        </div>
        <div class="c-register__footer">
            <a href="javascript:void(0);" data-step="5"
               class="c-button c-button c-button--outline JS--reg-prev-step-btn"><?php _e('Poprzednie', 'woofbox'); ?></a>
            <?php renderRegStepBullets(5); ?>
            <a href="javascript:void(0);" data-step="5"
               class="c-button JS--reg-next-step-btn is-disabled"><?php _e('Następne', 'woofbox'); ?></a>
        </div>
    </div>

    <div class="c-register__step JS--register-step JS--register-step-last c-register__step--last" data-register-step="6">
    <div class="c-register__row">
        <label for="name"><?php _e('Imię i Nazwisko właściciela', 'woofbox'); ?> </label>
        <input type="text" class="input-text" name="name" id="name" required
               value="<?php if (!empty($_POST['name'])) esc_attr_e($_POST['name']); ?>"/>
    </div>

    <div class="c-register__row">
        <label for="name"><?php _e('Miasto', 'woofbox'); ?> <span class="required">*</span></label>
        <input type="text" class="input-text" name="billing_city" id="billing_city" required
               value="<?php if (!empty($_POST['billing_city'])) esc_attr_e($_POST['billing_city']); ?>"/>
    </div>

    <!-- </div>-->
    <?php
}

add_action('woocommerce_register_form_start', 'wooc_extra_register_fields');

add_action('woocommerce_register_form_end', 'wooc_extra_register_fields_html_end');

function wooc_extra_register_fields_html_end()
{ ?>
    <div class="c-register__footer c-register__footer--last">
        <?php //renderRegStepBullets(6); ?>
        <a href="javascript:void(0);" data-step="6"
           class="c-button c-button c-button--outline JS--reg-prev-step-btn"><?php _e('Poprzednie', 'woofbox'); ?></a>
    </div>
    </div>
<?php }

// -------------------
// 2. Validate Fields

function wooc_validate_extra_register_fields($username, $email, $validation_errors)
{

    if (isset($_POST['dogs_name']) && empty($_POST['dogs_name'])) {
        $validation_errors->add('dogs_name', __('<strong>Error</strong>: Dog name is required!', 'woofbox'));
    }

    if (isset($_POST['breed']) && empty($_POST['breed'])) {
        $validation_errors->add('breed', __('<strong>Error</strong>: This field is required!', 'woofbox'));
    }
    if (isset($_POST['name']) && empty($_POST['name'])) {
        $validation_errors->add('name', __('<strong>Error</strong>: This field is required!', 'woofbox'));
    }


    return $validation_errors;
}

add_action('woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3);

// -------------------
// 3. Save fields on Customer Created action
add_action('woocommerce_created_customer', 'wooc_save_extra_register_fields');

function wooc_save_extra_register_fields($customer_id)
{
    if (isset($_POST['dog_sex'])) {
        update_user_meta($customer_id, 'sex', $_POST['dog_sex']);
    }
    if (isset($_POST['dogs_name'])) {
        update_user_meta($customer_id, 'dogs_name', sanitize_text_field($_POST['dogs_name']));
    }
    if (isset($_POST['dog-size'])) {
        update_user_meta($customer_id, 'size', $_POST['dog-size']);
    }

    if (isset($_POST['breed'])) {
        update_user_meta($customer_id, 'breed', sanitize_text_field($_POST['breed']));
    }

    if (isset($_POST['birthday'])) {
        update_user_meta($customer_id, 'birthday', $_POST['birthday']);
    }

    if (isset($_POST['treats'])) {
        update_user_meta($customer_id, 'treats', $_POST['treats']);
    }

    if (isset($_POST['name'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['name']));
    }

    if (isset($_POST['billing_city'])) {
        update_user_meta($customer_id, 'billing_city', sanitize_text_field($_POST['billing_city']));
    }

    if (isset($_POST['billing_city'])) {
        update_user_meta($customer_id, 'shipping_city', sanitize_text_field($_POST['billing_city']));
    }



}

// -------------------
// 4. Display Fields @ User Profile (admin) and My Account Edit page (front end)

add_action('show_user_profile', 'wooc_show_extra_register_fields', 30);
add_action('edit_user_profile', 'wooc_show_extra_register_fields', 30);
add_action('woocommerce_edit_account_form', 'wooc_show_extra_register_fields', 30);

function wooc_show_extra_register_fields($user)
{

    if (empty ($user)) {
        $user_id = get_current_user_id();
        $user = get_userdata($user_id);
        // Get attachment id
        $attachment_id = get_user_meta( $user_id, 'image', true );
        // True
        if ( $attachment_id ) {
            $original_image_url = wp_get_attachment_url( $attachment_id );

            // Display Image instead of URL
            echo'<div class="user-account-image-wrapper">';
            echo'<div class="user-account-image">';
            //echo wp_get_attachment_image( $attachment_id, 'full');
            echo '<img id="avatar-preview" src="'.$original_image_url.'">';
            echo'</div>';
        }else{
            echo'<div class="user-account-image-wrapper">';
            echo'<div class="user-account-image">';
            echo '<img id="avatar-preview" src="'.get_template_directory_uri().'/img/dog-placeholder.png">';
            echo '</div>';
        }
    }
    ?>
        <label for="dog-image">&nbsp;
            <input type="file"  id="dog-image" class="woocommerce-Input" name="image" accept="image/x-png,image/gif,image/jpeg">
        </label>

    </div> <!--End avatar-->

    <p class="form-row form-row-wide hidden-form-field">
        <label for="find_where"><?php _e('Jak ma na imię twoje zwierzę?', 'woofbox'); ?> <span class="required">*</span></label>
        <select name="dog_sex" id="dog_sex">
            <option value="boy" <?php if (get_the_author_meta('sex', $user->ID) == "boy") echo 'selected="selected" '; ?>><?php _e('Nasz chłopak ma na imię', 'woofbox'); ?></option>
            <option value="girl" <?php if (get_the_author_meta('sex', $user->ID) == "girl") echo 'selected="selected" '; ?>><?php _e('Nasza dziewczyna ma piękne imię.', 'woofbox'); ?></option>
        </select>
    </p>

    <p class="form-row form-row-wide">
        <input type="text" class="input-text" name="dogs_name" id="dogs_name" required
               value="<?php echo (get_the_author_meta('dogs_name', $user->ID)) ? get_the_author_meta('dogs_name', $user->ID) : ''; ?>"/>
    </p>



    <p class="form-row form-row-wide">
        <label for="breed"><?php _e('Jaki rodzaj rasy sdsa ?', 'woofbox'); ?> </label>
        <input type="text" class="input-text" name="breed" id="breed" required
               value="<?php echo (get_the_author_meta('breed', $user->ID)) ? get_the_author_meta('breed', $user->ID) : ''; ?>"/>
    </p>
    <p class="form-row form-row-wide">
        <label for="birthday"><?php _e('Kiedy są urodziny zwierzęcia sdsa ?', 'woofbox'); ?> </label>
        <input type="date" class="input-text" name="birthday" id="birthday"
               value="<?php echo (get_the_author_meta('birthday', $user->ID)) ? get_the_author_meta('birthday', $user->ID) : ''; ?>"/>
    </p>

    <div class="c-radio-dog-size">
        <label>
            <input type="radio" value="small"
                   name="dog-size" <?php echo (get_the_author_meta('size', $user->ID) == 'small') ? 'checked="checked"' : null; ?>>
            <div class="c-radio-dog-size__img">
                <img src="<?php echo get_template_directory_uri(); ?>/img/dogs/small_dog_size.png" alt="small dog">
            </div>
            <div class="c-radio-dog-size__text"> Mały i piękny. 0-10 kg</div>
        </label>
        <label>
            <input type="radio" value="medium"
                   name="dog-size" <?php echo (get_the_author_meta('size', $user->ID) == 'medium') ? 'checked="checked"' : null; ?>>
            <div class="c-radio-dog-size__img">
                <img src="<?php echo get_template_directory_uri(); ?>/img/dogs/medium_dog_size.png" alt="medium dog">
            </div>
            <div class="c-radio-dog-size__text"> Zgadza się. 10-20 kg</div>
        </label>
        <label>
            <input type="radio" value="big"
                   name="dog-size" <?php echo (get_the_author_meta('size', $user->ID) == 'big') ? 'checked="checked"' : null; ?>>
            <div class="c-radio-dog-size__img">
                <img src="<?php echo get_template_directory_uri(); ?>/img/dogs/large_dog_size.png" alt="large dog">
            </div>
            <div class="c-radio-dog-size__text"> Duży i miły. 25 + kg</div>
        </label>
    </div>

    <h2 class="c-register-radio-list__title text-center mt-4"><?php _e('Jeśli jest uczulony?', 'woofbox'); ?></h2>
    <h3 class="c-register-radio-list__subtitle text-center"><?php _e('Nie wysyłaj przysmaków zawierających...', 'woofbox'); ?></h3>

    <div class="c-register-radio-list">
     <div class="c-register-radio-list__item">
            <input type="radio" name="treats" value="chicken" id="chicken"
                   class="c-register-radio-list__input" <?php echo (get_the_author_meta('treats', $user->ID) == 'chicken') ? 'checked="checked"' : null; ?>>
            <label for="chicken"><?php _e('Kurczak', 'woofbox'); ?></label>
        </div>
        <div class="c-register-radio-list__item">
            <input type="radio" name="treats" value="turkey" id="turkey"
                   class="c-register-radio-list__input" <?php echo (get_the_author_meta('treats', $user->ID) == 'turkey') ? 'checked="checked"' : null; ?>>
            <label for="turkey"><?php _e('Indyk', 'woofbox'); ?></label>
        </div>
        <div class="c-register-radio-list__item">
            <input type="radio" name="treats" value="beef" id="beef"
                   class="c-register-radio-list__input" <?php echo (get_the_author_meta('treats', $user->ID) == 'beef') ? 'checked="checked"' : null; ?>>
            <label for="beef"><?php _e('Wołowina', 'woofbox'); ?></label>
        </div>
        <div class="c-register-radio-list__item">
            <input type="radio" name="treats" value="none" id="no_send_me_everything"
                   class="c-register-radio-list__input" <?php echo (get_the_author_meta('treats', $user->ID) == 'none') ? 'checked="checked"' : null; ?>>
            <label for="no_send_me_everything"><?php _e('Nie, przyślij mi wszystko.', 'woofbox'); ?></label>
        </div>
    </div>


    <?php

}

// -------------------
// 5. Save User Field When Changed From the Admin/Front End Forms

add_action('personal_options_update', 'wooc_save_extra_register_fields_admin');
add_action('edit_user_profile_update', 'wooc_save_extra_register_fields_admin');
add_action('woocommerce_save_account_details', 'wooc_save_extra_register_fields_admin');

function wooc_save_extra_register_fields_admin($customer_id)
{
    if (isset($_POST['dog_sex'])) {
        update_user_meta($customer_id, 'sex', $_POST['dog_sex']);
    }

    if (isset($_POST['dogs_name'])) {
        update_user_meta($customer_id, 'dogs_name', sanitize_text_field($_POST['dogs_name']));
    }

    if (isset($_POST['dog-size'])) {
        update_user_meta($customer_id, 'size', $_POST['dog-size']);
    }

    if (isset($_POST['breed'])) {
        update_user_meta($customer_id, 'breed', sanitize_text_field($_POST['breed']));
    }

    if (isset($_POST['birthday'])) {
        update_user_meta($customer_id, 'birthday', $_POST['birthday']);
    }
    if (isset($_POST['treats'])) {
        update_user_meta($customer_id, 'treats', $_POST['treats']);
    }

    if (isset($_POST['name'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['name']));
    }

    if ( isset( $_FILES['image'] ) ) {
        require_once( ABSPATH . 'wp-admin/includes/image.php' );
        require_once( ABSPATH . 'wp-admin/includes/file.php' );
        require_once( ABSPATH . 'wp-admin/includes/media.php' );

        $attachment_id = media_handle_upload( 'image', 0 );

        if ( is_wp_error( $attachment_id ) ) {
            update_user_meta( $customer_id, 'image', $_FILES['image'] . ": " . $attachment_id->get_error_message() );
        } else {
            update_user_meta( $customer_id, 'image', $attachment_id );
        }
    }


}

// Add enctype to form to allow image upload
function woof_action_woocommerce_edit_account_form_tag() {
    echo 'enctype="multipart/form-data"';
}
add_action( 'woocommerce_edit_account_form_tag', 'woof_action_woocommerce_edit_account_form_tag' );

// Shipping fields
/* Add additional shipping fields (email, phone) in FRONT END (i.e. My Account and Order Checkout) */
/* Note:  $fields keys (i.e. field names) must be in format: "shipping_" */
add_filter( 'woocommerce_shipping_fields' , 'my_additional_shipping_fields',10,2 );
function my_additional_shipping_fields( $fields ) {
    $fields['shipping_email'] = array(
        'label'         => __( 'Ship Email', 'woocommerce' ),
        'required'      => false,
        'class'         => array( 'form-row-first' ),
        'priority'     => 20,
        'validate'      => array( 'email' ),
    );
    $fields['shipping_phone'] = array(
        'label'         => __( 'Ship Phone', 'woocommerce' ),
        'required'      => true,
        'class'         => array( 'form-row-last' ),
        'clear'         => true,
        'priority'     => 20,
        'validate'      => array( 'phone' ),
    );
    return $fields;
}
/* Display additional shipping fields (email, phone) in ADMIN area (i.e. Order display ) */
/* Note:  $fields keys (i.e. field names) must be in format:  WITHOUT the "shipping_" prefix (it's added by the code) */
add_filter( 'woocommerce_admin_shipping_fields' , 'my_additional_admin_shipping_fields' );
function my_additional_admin_shipping_fields( $fields ) {
    $fields['email'] = array(
        'label' => __( 'Order Ship Email', 'woocommerce' ),
    );
    $fields['phone'] = array(
        'label' => __( 'Order Ship Phone', 'woocommerce' ),
    );
    return $fields;
}
/* Display additional shipping fields (email, phone) in USER area (i.e. Admin User/Customer display ) */
/* Note:  $fields keys (i.e. field names) must be in format: shipping_ */
add_filter( 'woocommerce_customer_meta_fields' , 'my_additional_customer_meta_fields' );
function my_additional_customer_meta_fields( $fields ) {
    $fields['shipping']['fields']['shipping_phone'] = array(
        'label' => __( 'Telephone', 'woocommerce' ),
        'description' => '',
    );
    $fields['shipping']['fields']['shipping_email'] = array(
        'label' => __( 'Email', 'woocommerce' ),
        'description' => '',
    );
    return $fields;
}












function renderRegStepBullets($currentStep)
{
    echo '<ul class="c-register__bullets">';
    echo '<li class="c-register__bullet '.__(($currentStep == 1) ? "is-active" : '').'"></li>';
    echo '<li class="c-register__bullet '.__(($currentStep == 2) ? "is-active" : '').'"></li>';
    echo '<li class="c-register__bullet '.__(($currentStep == 3) ? "is-active" : '').'"></li>';
    echo '<li class="c-register__bullet '.__(($currentStep == 4) ? "is-active" : '').'"></li>';
    echo '<li class="c-register__bullet '.__(($currentStep == 5) ? "is-active" : '').'"></li>';
    echo '<li class="c-register__bullet '.__(($currentStep == 6) ? "is-active" : '').'"></li>';

    echo '</ul>';
}