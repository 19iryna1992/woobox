<?php

if (!defined('ABSPATH')) exit;

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wfb_widgets_init()
{

    register_sidebar(array(
        'name' => esc_html__('Footer Column 1', 'woofbox'),
        'id' => 'footer-1',
        'description' => esc_html__('Add widgets here.', 'woofbox'),
        'before_widget' => '<div id="%1$s" class="c-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="c-widget__title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Column 2', 'woofbox'),
        'id' => 'footer-2',
        'description' => esc_html__('Add widgets here.', 'woofbox'),
        'before_widget' => '<div id="%1$s" class="c-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="c-widget__title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer Column 3', 'woofbox'),
        'id' => 'footer-3',
        'description' => esc_html__('Add widgets here.', 'woofbox'),
        'before_widget' => '<div id="%1$s" class="c-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="c-widget__title">',
        'after_title' => '</h2>',
    ));

}

add_action('widgets_init', 'wfb_widgets_init');